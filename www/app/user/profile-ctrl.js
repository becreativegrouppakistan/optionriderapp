(function() {
    'use strict';

    angular
        .module('MaduberRiderApp')
        .controller('ProfileCtrl', ['$scope', '$location', 'ionicToast', 'UserServices', 'loading', 'currentUser', '$ionicModal', ProfileCtrl]);

    /* @ngInject */
    function ProfileCtrl($scope, $location, ionicToast, UserServices, loading, currentUser, $ionicModal) {
        var vm = this;

        vm.rider = {
          rider_id: null,
            name: '',
            email: '',
            phone_no: '',
            address: '',
           licence_no:''
        }

        function init() {
            if (currentUser.profile()) {
                vm.user = currentUser.profile();
                vm.rider = {
                    rider_id: vm.user.rider_id,
                    name: vm.user.name,
                    email: vm.user.email,
                    phone_no: vm.user.phone_no,
                    address: vm.user.address,
                    licence_no: vm.user.licence_no
                }
            }
        }
        init();

        $ionicModal.fromTemplateUrl('app/user/edit-profile.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });
        vm.openModal = function() {
            $scope.modal.show();
        };
        vm.closeModal = function() {
            $scope.modal.hide();
        };


        vm.update = function(user) {
            vm.spinner = true;
            UserServices.update(user)
                .then(forgotSuccess)
                .catch(forgotError);
        };

        vm.updateProfile = function() {
            loading.show();
            UserServices.update(vm.rider)
                .then(function(response) {
                    currentUser.save(response.data.data); // save data

                    ionicToast.show(response.data.msg, 'top', false, 4000);
                    loading.hide();
                    vm.closeModal();
                })
                .catch(function(response) {
                    loading.hide();
                    ionicToast.show("Unable to connect to the server", 'top', false, 4000);
                });
        };

        vm.logout = function() {

            UserServices.logout();
            $location.path('/app/login');

        };

    }
})();
