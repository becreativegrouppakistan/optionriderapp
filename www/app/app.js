angular.module('MaduberRiderApp', ['ionic', 'ngCordova', 'ngMap', 'ionic-toast'])

.run(function($ionicPlatform, $cordovaBadge, $ionicPopup, $location) {

    // $scope.$emit('someEvent', someValue(s));

    var count = 0;
    $ionicPlatform.ready(function() {
        cordova.plugins.backgroundMode.enable();
        $cordovaBadge.hasPermission().then(function(yes) {
            $cordovaBadge.set(count).then(function() {
                // You have permission, badge set.
            }, function(err) {
                // You do not have permission.
            });
        }, function(no) {
            // You do not have permission
        });
        window.FirebasePlugin.onNotificationOpen(function(notification) {
                console.log(notification);
                if (notification) {
                    count += 1;
                    console.log(count);
                    $cordovaBadge.increase(count).then(function() {
                        // You have permission, badge increased.
                    }, function(err) {
                        // You do not have permission.
                    });
                    $cordovaBadge.get().then(function(badge) {
                        console.log(badge);
                        // You have permission, badge returned.
                    }, function(err) {
                        // You do not have permission.
                    });
                }
                if (notification.wasTapped) {
                    $cordovaBadge.clear().then(function() {
                        // You have permission, badge cleared.
                    }, function(err) {
                        // You do not have permission.
                    });
                    $location.path('app/view_order/' + notification.order_id);
                    //Notification was received on device tray and tapped by the user.
                    // alert("Tapped: " +  JSON.stringify(data) );

                } else {
                    $cordovaBadge.clear().then(function() {
                        // You have permission, badge cleared.
                    }, function(err) {
                        // You do not have permission.
                    });
                    //Notification was received in foreground. Maybe the user needs to be notified.
                    // alert("Not tapped: " + JSON.stringify(data));
                    var myPopup = $ionicPopup.show({
                        template: notification.body,
                        title: notification.title,
                        buttons: [
                            { text: 'Cancel' },
                            {
                                text: '<b>See</b>',
                                type: 'button-positive',
                                onTap: function(e) {
                                    $location.path('app/view_order/' + notification.order_id);
                                }
                            }
                        ]
                    });
                }

            },
            function(error) {
                console.error(error);
            });
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
            StatusBar.backgroundColorByHexString("#CE202D");
        }
    });
})

.config(['$stateProvider', '$urlRouterProvider', 'currentUserProvider', '$ionicConfigProvider', function($stateProvider, $urlRouterProvider, currentUserProvider, $ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.navBar.alignTitle('center');
    $stateProvider

    // setup an abstract state for the tabs directive
        .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'app/layout/tabs.html'
    })

    // Each tab has its own nav history stack:

    .state('app.orders', {
        url: '/orders',
        views: {
            'app-orders': {
                templateUrl: 'app/orders/my_orders.html'
            }
        }
    })

    .state('app.view_order', {
        url: '/view_order/:order_id',
        views: {
            'app-orders': {
                templateUrl: 'app/orders/view_order.html'
            }
        }
    })

    .state('app.history', {
            url: '/history',
            views: {
                'app-history': {
                    templateUrl: 'app/history/history.html'
                }
            }
        })
        .state('app.view_history_order', {
            url: '/view_history_order/:order_id',
            views: {
                'app-history': {
                    templateUrl: 'app/history/view_history_order.html'
                }
            }
        })

    .state('app.user', {
        url: '/user',
        views: {
            'app-user': {
                templateUrl: 'app/user/profile.html'
            }
        }
    })

    // setup an abstract state for the tabs directive
    .state('user', {
        url: '/user',
        abstract: true,
        templateUrl: 'app/layout/user.html'
    })

    // Each tab has its own nav history stack:

    .state('user.login', {
        url: '/login',
        views: {
            'user': {
                templateUrl: 'app/user/login.html'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    if (currentUserProvider.$get().is_loggedin()) {
        $urlRouterProvider.otherwise('/app/orders');

    } else {
        $urlRouterProvider.otherwise('/user/login');
    }

}]);