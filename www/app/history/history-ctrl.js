(function() {
    'use strict';

    angular
        .module('MaduberRiderApp')
        .controller('OrderHistoryCtrl', ['OrderServices', 'currentUser', 'loading', '$scope', OrderHistoryCtrl]);

    /* @ngInject */
    function OrderHistoryCtrl(OrderServices, currentUser, loading, $scope) {
        var vm = this;

        // vm.activate();

        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {

            if (toState.name == 'app.history') {
                vm.activate();
            }
        });
        vm.activate = function() {
            loading.show();
            vm.orders = {};
            OrderServices
                .orders(parseInt(currentUser.profile().rider_id))
                .then(function(resp) {
                    vm.orders = resp.data.data;
                    angular.forEach(vm.orders, function(option) {
                        vm.date = option.created_at;
                        vm.date = new Date().toLocaleString();
                    });
                    loading.hide();
                })
                .catch(function(resp) {
                    console.log(resp);
                    loading.hide();
                });
        }


        vm.statusClass = function(status) {
            if (status == 0) {
                return "placed";
            } else if (status == 1) {
                return "reject";
            } else if (status == 2) {
                return "accept";
            } else if (status == 3) {
                return "preparing";
            } else if (status == 4) {
                return "delivering_status";
            } else if (status == 4) {
                return "Delivered";
            }

        };

        vm.OrderStatus = function(status) {
            if (status == 0) {
                vm.statusClass = "placed";
                return "Order Placed";
            } else if (status == 1) {
                vm.statusClass = "reject";
                return "Reject";
            } else if (status == 2) {
                vm.statusClass = "accept";
                return "Accept";
            } else if (status == 3) {
                vm.statusClass = "preparing";
                return "Preparing";
            } else if (status == 4) {
                vm.statusClass = "delivering_status";
                return "Delivery";
            } else if (status == 5) {
                vm.statusClass = "Delivered";
                return "Delivered";
            }
        };


        vm.OrderPayment = function(payment) {
            if (payment == 0) {
                return 'not paid';
            } else if (payment == 1) {
                return 'Paid';
            }
        };

        vm.doRefresh = function() {
            vm.orders = {};
            loading.show();
            OrderServices
                .orders(parseInt(currentUser.profile().rider_id))
                .then(function(resp) {
                    vm.orders = resp.data.data;
                    loading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                })
                .catch(function(resp) {
                    console.log(resp);
                    loading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };
    }
})();