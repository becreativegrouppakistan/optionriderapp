(function() {
    'use strict';

    angular
        .module('MaduberRiderApp')
        .factory('MyHTTP', ['$http', 'configuration', 'formEncode', 'LocalStorage', 'currentUser', MyHTTP]);

    /* @ngInject */
    function MyHTTP($http, configuration, formEncode, LocalStorage, currentUser) {
        var service = {
            post: post,
            post1: post1,
            get: get
        };
        return service;

        ////////////////

        function post(data, url) {
            var VENUE_ID = LocalStorage.get('VENUEID');

            return $http({
                method: 'POST',
                url: configuration.API_URL + url + '/venue_id/' + VENUE_ID,
                data: formEncode(data),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });
        }

        function post1(data, url) {
            console.log(data);
            return $http({
                method: 'POST',
                url: configuration.API_URL + url,
                data: formEncode(data),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });
        }

        function get(url) {
            return $http({
                method: 'GET',
                url: configuration.API_URL + url
            });
        }

        function get1(url) {
            return $http({
                method: 'GET',
                url: configuration.API_URL + url + '?venue_id=' + configuration.VENUE_ID
            });
        }
    }
})();
