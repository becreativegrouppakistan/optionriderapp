﻿(function() {
    'use strict';

    angular
        .module('MaduberRiderApp')
        .factory('LocalStorage', ['$window', LocalStorage]);

    function LocalStorage($window) {

        var store = $window.localStorage;

        return {
            add: add,
            get: get,
            remove: remove
        };

        function add(key, value) {
            value = angular.toJson(value);
            store.setItem('rider_'+key, value);

        }

        function get(key) {
            var value = store.getItem('rider_'+key);
            if (value) {
                value = angular.fromJson(value);
            }
            return value;
        }

        function remove(key) {
            store.removeItem('rider_'+key);
        }
    }

})();
