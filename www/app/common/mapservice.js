(function() {
        'use strict';

        angular
            .module('MaduberRiderApp')
            .factory('mapservice', [mapservice]);


        function mapservice() {
            var bgOptions = {
                stationaryRadius: 50,
                distanceFilter: 50,
                desiredAccuracy: 10,
                debug: true,
                notificationTitle: 'Background tracking',
                notificationText: 'disabled',
                notificationIconColor: '#FEDD1E',
                notificationIconLarge: 'mappointer_large',
                notificationIconSmall: 'mappointer_small',
                locationProvider: 0, //backgroundGeolocation.provider.ANDROID_DISTANCE_FILTER_PROVIDER,
                interval: 10,
                fastestInterval: 5,
                activitiesInterval: 10,
                stopOnTerminate: false,
                startOnBoot: false,
                startForeground: true,
                stopOnStillActivity: true,
                activityType: 'AutomotiveNavigation',
                url: 'http://192.168.81.15:3000/locations',
                syncUrl: 'http://192.168.81.15:3000/sync',
                syncThreshold: 100,
                httpHeaders: {
                    'X-FOO': 'bar'
                },
                pauseLocationUpdates: false,
                saveBatteryOnBackground: false,
                maxLocations: 100
            };

            function bgConfigure(config) {
                Object.assign(bgOptions, config);
                localStorage.setItem('bgOptions', JSON.stringify(bgOptions));

                var options = Object.assign({}, bgOptions);
                if (options.interval) { options.interval *= 1000; }
                if (options.fastestInterval) { options.fastestInterval *= 1000; }
                if (options.activitiesInterval) { options.activitiesInterval *= 1000; }

                if (isStarted) {
                    stopTracking();
                    backgroundGeolocation.configure(
                        setCurrentLocation,
                        function(err) { console.log('Error occured', err); },
                        options
                    );
                    startTracking();
                } else {
                    backgroundGeolocation.configure(
                        setCurrentLocation,
                        function(err) { console.log('Error occured', err); },
                        options
                    );
                }
            }

            function startTracking() {
                if (isStarted) { return; }

                backgroundGeolocation.isLocationEnabled(
                    function(enabled) {
                        isLocationEnabled = enabled;
                        if (enabled) {
                            backgroundGeolocation.start(
                                null,
                                function(error) {
                                    stopTracking();
                                    if (error.code === 2) {
                                        myApp.confirm('Would you like to open app settings?', 'Permission denied', function() {
                                            backgroundGeolocation.showAppSettings();
                                        });
                                    } else {
                                        myApp.alert(error.message, 'Start failed');
                                    }
                                }
                            );
                            isStarted = true;
                            renderTabBar(isStarted);
                        } else {
                            myApp.confirm('Would you like to open settings?', 'Location Services are disabled', function() {
                                backgroundGeolocation.showLocationSettings();
                            });
                        }
                    },
                    function(error) {
                        myApp.alert(error, 'Error detecting status of location settings');
                    }
                );
            }

            function stopTracking() {
                if (!isStarted) { return; }
                // userStartIntent = false;

                backgroundGeolocation.stop();
                isStarted = false;
                renderTabBar(isStarted);
            }

            function setCurrentLocation(location) {
                console.log('[DEBUG] location recieved', location);
                if (!currentLocationMarker) {
                    currentLocationMarker = new google.maps.Marker({
                        map: map,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: 7,
                            fillColor: 'gold',
                            fillOpacity: 1,
                            strokeColor: 'white',
                            strokeWeight: 3
                        }
                    });
                    locationAccuracyCircle = new google.maps.Circle({
                        fillColor: 'purple',
                        fillOpacity: 0.4,
                        strokeOpacity: 0,
                        map: map
                    });
                }
                if (!path) {
                    path = new google.maps.Polyline({
                        map: map,
                        strokeColor: 'blue',
                        fillOpacity: 0.4
                    });
                }
                var latlng = new google.maps.LatLng(Number(location.latitude), Number(location.longitude));

                if (previousLocation) {
                    // Drop a breadcrumb of where we've been.
                    locationMarkers.push(new google.maps.Marker({
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: 7,
                            fillColor: 'green',
                            fillOpacity: 1,
                            strokeColor: 'white',
                            strokeWeight: 3
                        },
                        map: map,
                        position: new google.maps.LatLng(previousLocation.latitude, previousLocation.longitude)
                    }));
                } else {
                    map.setCenter(latlng);
                    if (map.getZoom() < 15) {
                        map.setZoom(15);
                    }
                }

                // Update our current position marker and accuracy bubble.
                currentLocationMarker.setPosition(latlng);
                locationAccuracyCircle.setCenter(latlng);
                locationAccuracyCircle.setRadius(location.accuracy);

                // Add breadcrumb to current Polyline path.
                path.getPath().push(latlng);
                previousLocation = location;

                backgroundGeoLocation.finish();
            }

            function onDeviceReady() {
                backgroundGeolocation = window.backgroundGeolocation || window.backgroundGeoLocation || window.universalGeolocation;
                backgroundGeolocation.getLocations(function(locs) {
                    var now = Date.now();
                    console.log(locs);
                    var sameDayDiffInMillis = 24 * 3600 * 1000;
                    locs.forEach(function(loc) {
                        if ((now - loc.time) <= sameDayDiffInMillis) {
                            setCurrentLocation(loc);
                        }
                    });
                });
            }
        })();