(function() {
    'use strict';

    angular
        .module('MaduberRiderApp')
        .factory('UserServices', ['MyHTTP', 'currentUser', 'setting', UserServices]);

    function UserServices(MyHTTP, currentUser, setting) {

        return {
            login: login,
            logout: logout,
            update: update,
            getCustomer: getCustomer
        };
        // returen customer by customer_id
        function getCustomer() {
            return MyHTTP.get('customer/get/customer_id/' + currentUser.profile().customer_id);
        }

        // submit customer form to server
        function update(user) {
            return MyHTTP.post1(user, 'update_rider?rider_id=' + currentUser.profile().rider_id);
        }

        // login user
        function login(user) {
            return MyHTTP.post(user, 'api_v2/login');
        }


        // forgot password
        function updateProfilePic(user) {
            return MyHTTP.post(user, 'customer/forgot_password');
        }

        // logout user
        function logout() {
            currentUser.profile.rider_id = "";
            currentUser.profile.email = "";
            currentUser.profile.first_name = "";
            currentUser.profile.last_name = "";
            currentUser.profile.phone_no = "";
            currentUser.profile.is_loggedin = "";
            currentUser.remove();
        }

    } //
})();
