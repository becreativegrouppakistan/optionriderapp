(function() {
    'use strict';

    angular
        .module('MaduberRiderApp')
        .controller('ViewOrderCtrl', ['$scope', 'OrderServices', '$stateParams', 'setting', 'loading', 'currentUser', '$ionicScrollDelegate', '$cordovaGeolocation', '$ionicHistory', 'LocalStorage', ViewOrderCtrl]);

    /* @ngInject */
    function ViewOrderCtrl($scope, OrderServices, $stateParams, setting, loading, currentUser, $ionicScrollDelegate, $cordovaGeolocation, $ionicHistory, LocalStorage) {
        var vm = this;
        vm.tab = 1;
        vm.title = 'View Order(' + $stateParams.order_id + ')';
        vm.mapLoaded = false;
        vm.order = { status: false };
        vm.chatMsgs = {};
        vm.starttrack = false;
        vm.isStarted = false,
            vm.isLocationEnabled = false,
            vm.bgOptions = {
                stationaryRadius: 50,
                distanceFilter: 50,
                desiredAccuracy: 10,
                debug: true,
                locationProvider: 0, //backgroundGeolocation.provider.ANDROID_DISTANCE_FILTER_PROVIDER,
                interval: 10,
                fastestInterval: 5,
                activitiesInterval: 10,
                stopOnTerminate: false,
                startOnBoot: false,
                startForeground: true,
                stopOnStillActivity: true,
                activityType: 'AutomotiveNavigation',
                pauseLocationUpdates: false,
                saveBatteryOnBackground: false,
            };

        function activate() {
            vm.setting = setting.venue_setting();
            loading.show();
            vm.orders = {};
            LocalStorage.add("order_id", $stateParams.order_id)
            OrderServices
                .orders(parseInt(currentUser.profile().rider_id))
                .then(function(resp) {
                    vm.orders = resp.data.data;
                    angular.forEach(vm.orders, function(option) {
                        if ($stateParams.order_id == option.order_id) {
                            vm.order = option;
                            console.log(vm.order);
                            vm.initMap();
                        }
                    });
                    loading.hide();
                })
                .catch(function(resp) {
                    console.log(resp);
                    loading.hide();
                });

        }
        activate();
        vm.statusClass = "";
        vm.orderStatusData = {
            'order_placed': false,
            'reject': false,
            'accept': false,
            'preparing': false,
            'delivery': false,
            'delivered': false
        };
        vm.OrderStatus = function(status) {
            if (status == 0) {
                vm.NotallowFeedback = true;
                vm.orderStatusData = {
                    'order_placed': true,
                    'reject': false,
                    'accept': false,
                    'preparing': false,
                    'delivery': false,
                    'delivered': false
                };

                vm.statusClass = "order_placed";
                return "Order placed";
            } else if (status == 1) {
                vm.NotallowFeedback = true;
                vm.orderStatusData = {
                    'order_placed': true,
                    'reject': true,
                    'accept': false,
                    'preparing': false,
                    'delivery': false,
                    'delivered': false
                };
                vm.statusClass = "reject";
                return "Reject";
            } else if (status == 2) {
                vm.NotallowFeedback = true;
                vm.orderStatusData = {
                    'order_placed': true,
                    'reject': false,
                    'accept': true,
                    'preparing': false,
                    'delivery': false,
                    'delivered': false
                };
                vm.statusClass = "accept";
                return "Accept";
            } else if (status == 3) {
                vm.NotallowFeedback = true;
                vm.orderStatusData = {
                    'order_placed': true,
                    'reject': false,
                    'accept': true,
                    'preparing': true,
                    'delivery': false,
                    'delivered': false
                };
                vm.statusClass = "preparing";
                return "Preparing";
            } else if (status == 4) {
                vm.NotallowFeedback = true;
                vm.orderStatusData = {
                    'order_placed': true,
                    'reject': false,
                    'accept': true,
                    'preparing': true,
                    'delivery': true,
                    'delivered': false
                };
                vm.statusClass = "delivery";
                return "Delivery";
            } else if (status == 5) {

                vm.NotallowFeedback = false;
                vm.orderStatusData = {
                    'order_placed': true,
                    'reject': false,
                    'accept': true,
                    'preparing': true,
                    'delivery': true,
                    'delivered': true
                };
                vm.statusClass = "Delivered";
                return "Delivered";
            }
        };


        vm.OrderPayment = function(payment) {
            if (payment == 0) {
                return 'not paid';
            } else if (payment == 1) {
                return 'paid';
            }
        };


        //////// Change Order Status ///////////

        vm.onclickstatus = function(order_id, status) {
            loading.show();
            var data = {
                order_id: order_id,
                status: status
            };
            OrderServices.update_order_status(data).then(function(res) {
                loading.hide();
                if (res.data) {
                    firebase.database()
                        .ref('orders/' + order_id)
                        .set({ 'order_id': order_id, 'status': status });
                    firebase.database()
                        .ref('venueorderupdate')
                        .child(LocalStorage.get('VENUEID'))
                        .push(new Date().getUTCMilliseconds());
                    $ionicHistory.goBack();
                    activate();
                }
            }).catch().finally(function() {
                loading.show();
                // $scope.modal.hide();
            });
        }


        vm.unreadCount = 0;
        vm.chatmsg = {
            'name': currentUser.profile().name + "(rider)",
            'type': 2,
            'msg': '',
            'timestamp': Date.now()
        };
        vm.chatMsgSend = function() {
            firebase.database()
                .ref('chat')
                .child($stateParams.order_id)
                .push(vm.chatmsg);
            $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(true);
            vm.chatmsg.msg = '';
        }

        vm.chatScrollBottom = function() {
            vm.unreadCount = 0;
            $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(true);
        }

        firebase.database().ref('chat/' + $stateParams.order_id)
            .on('value', function(data) {
                if (vm.tab !== 4) {
                    vm.unreadCount = vm.unreadCount + 1;
                }
                vm.chatMsgs = data.val();
                vm.chatScrollBottom();
            });

        vm.initMap = function() {
            var venueLatLng = new google.maps.LatLng(vm.order.venue_address.venue_latitude, vm.order.venue_address.venue_longitude);
            var mapOptions = {
                center: venueLatLng,
                zoom: 15,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                fullscreenControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            vm.map = map;

            vm.setDirections(vm.order);

            google.maps.event.addListener(map, 'bounds_changed', function() {
                vm.mapLoaded = true;
                google.maps.event.trigger(map, 'resize');
            });
        }


        vm.makeMarker = function(position, icon, title, map) {
            var marker = new google.maps.Marker({
                position: position,
                map: map,
                icon: icon,
                title: title
            });
            return marker;
        };



        vm.setDirections = function(order) {
            var start = null;
            var end = null;
            var icons = {
                start: new google.maps.MarkerImage(
                    // URL
                    'http://maps.google.com/mapfiles/ms/micons/blue.png',
                    // (width,height)
                    new google.maps.Size(44, 32),
                    // The origin point (x,y)
                    new google.maps.Point(0, 0),
                    // The anchor point (x,y)
                    new google.maps.Point(0, 0)),
                end: new google.maps.MarkerImage(
                    // URL
                    'http://maps.google.com/mapfiles/ms/micons/green.png',
                    // (width,height)
                    new google.maps.Size(44, 32),
                    // The origin point (x,y)
                    new google.maps.Point(0, 0),
                    // The anchor point (x,y)
                    new google.maps.Point(22, 32))
            };

            if (vm.order.venue_address.venue_latitude !== '') {
                start = new google.maps.LatLng(vm.order.venue_address.venue_latitude, vm.order.venue_address.venue_longitude);
            } else {
                start = vm.order.venue_address.venue_address;
            }

            if (vm.order.customer_address.latitude !== '' && vm.order.customer_address.longitude !== '') {
                end = new google.maps.LatLng(vm.order.customer_address.latitude, vm.order.customer_address.longitude);
            } else {
                end = vm.order.customer_address.address;
            }

            //  end = new google.maps.LatLng(37.35728082 , -122.11715581)

            var rendererOptions = {
                map: vm.map,
                suppressMarkers: true
            };

            var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

            directionsDisplay.setMap(vm.map);
            var request = {
                origin: start,
                destination: end,
                travelMode: 'DRIVING'
            };
            var directionsService = new google.maps.DirectionsService();
            directionsService.route(request, function(response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);

                    var leg = response.routes[0].legs[0];
                    vm.start_marker = vm.makeMarker(leg.start_location, icons.start, "title", vm.map);
                    vm.end_marker = vm.makeMarker(leg.end_location, icons.end, 'title', vm.map);

                }
            });

            if (vm.starttrack === false && vm.mapLoaded) {

                firebase.database().ref('rider_location/' + $stateParams.order_id)
                    .on('value', function(data) {
                        vm.rider_location = data.val();
                        if (vm.rider_location) {
                            var riderPosition = new google.maps.LatLng(vm.rider_location.latitude, vm.rider_location.longitude);

                            var riderIcon = {
                                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                                fillColor: '#e8202d',
                                fillOpacity: 0.8,
                                strokeColor: '#e8202d',
                                strokeOpacity: 0.4,
                                scale: 5
                            };
                            if (angular.isNumber(vm.rider_location.heading)) {
                                riderIcon.rotation = vm.rider_location.heading;
                                vm.map.setHeading(vm.rider_location.heading);
                            }
                            vm.start_marker.setIcon(riderIcon);

                            vm.start_marker.setPosition(riderPosition);

                            var request = {
                                origin: riderPosition,
                                destination: end,
                                travelMode: 'DRIVING'
                            };
                            directionsService.route(request, function(response, status) {
                                console.log(response);
                                if (status === google.maps.DirectionsStatus.OK) {
                                    directionsDisplay.setDirections(response);

                                }
                            });
                            // if(angular.isNumber(vm.rider_location.heading)){
                            //   vm.map.setHeading(vm.rider_location.heading);
                            // }
                        }
                    });
            }
        }


        // if (LocalStorage.get("order_id")) {
        //   firebase.database()
        //     .ref('rider_location')
        //     .child(LocalStorage.get("order_id"))
        //     .update(data);
        // }

        // $scope.$on('someEvent', function(event, data) { console.log(data); });

        //////////////// Background Geolocaion

        // BackgroundGeolocation is highly configurable. See platform specific configuration options
        vm.bgConfigure = function bgConfigure(config) {
            if (vm.isStarted) {
                vm.stopTracking();
                backgroundGeolocation.configure(
                    vm.setCurrentLocation,
                    function(err) { console.log('Error occured', err); },
                    vm.bgOptions
                );
                vm.startTracking();
            } else {
                backgroundGeolocation.configure(
                    vm.setCurrentLocation,
                    function(err) { console.log('Error occured', err); },
                    vm.bgOptions
                );
            }
        }

        vm.setCurrentLocation = function(location) {
            console.log('[DEBUG] location recieved', location);
            firebase.database()
                .ref('rider_location')
                .child($stateParams.order_id)
                .update(location);
            console.log(location);
            backgroundGeoLocation.finish();
        }

        vm.startTracking = function() {
            if (vm.isStarted) { return; }
            vm.isStarted = true;
            backgroundGeolocation.isLocationEnabled(
                function(enabled) {
                    vm.isLocationEnabled = enabled;
                    vm.isStarted = true;
                    backgroundGeolocation.start(
                        null,
                        function(error) {
                            vm.stopTracking();
                            console.log(error, 'Start failed');
                        },
                    );
                    vm.isStarted = true;
                },
                function(error) {
                    console.log(error, 'Error detecting status of location settings');
                }
            );
        }

        vm.stopTracking = function() {
            if (!vm.isStarted) { return; }
            // userStartIntent = false;

            backgroundGeolocation.stop();
            vm.isStarted = false;
            // renderTabBar(vm.isStarted);
        }

        document.addEventListener("deviceready", function() {
            backgroundGeolocation.switchMode(backgroundGeolocation.mode.BACKGROUND);
            vm.starttrack == true;
            /**
             * This callback will be executed every time a geolocation is recorded in the background.
             */


            backgroundGeolocation.watchLocationMode(
                function(enabled) {
                    isLocationEnabled = enabled;
                    if (enabled && userStartIntent) {
                        vm.startTracking();
                    } else if (vm.isStarted) {
                        vm.stopTracking();
                        console.log('Location tracking has been stopped');
                    }
                },
                function(error) {
                    console.log(error, 'Error watching location mode');
                }
            );

            backgroundGeolocation.configure(
                vm.setCurrentLocation,
                function(err) { console.log('Error occured', err); },
                vm.bgOptions
            );

            backgroundGeolocation.getLocations(function(locs) {
                var now = Date.now();
                console.log(locs);
                var sameDayDiffInMillis = 24 * 3600 * 1000;
                locs.forEach(function(loc) {
                    if ((now - loc.time) <= sameDayDiffInMillis) {
                        vm.setCurrentLocation(loc);
                    }
                });
            });

        }, false);

    }
})();