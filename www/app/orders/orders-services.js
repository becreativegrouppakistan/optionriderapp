(function() {
    'use strict';

    angular
        .module('MaduberRiderApp')
        .factory('OrderServices', ['MyHTTP', 'setting', OrderServices]);

    /* @ngInject */
    function OrderServices(MyHTTP) {
        var orders = {
            orders: orders,
            update_order_status: update_order_status,
            change_order_new_status: change_order_new_status
        };
        return orders;

        ////////////////

        function orders(rider_id) {
            return MyHTTP.get('get_orders?rider_id=' + rider_id);
        }

        function update_order_status(data) {
            return MyHTTP.post1(data, 'update_order_status');
        }

        function change_order_new_status(data) {
            return MyHTTP.post1(data, 'change_order_new_status');
        }
    }
})();
