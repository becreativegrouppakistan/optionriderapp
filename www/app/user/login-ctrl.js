(function() {
    'use strict';

    angular.module('MaduberRiderApp').controller('LoginCtrl', ['$scope', '$location', 'MyHTTP', 'currentUser', 'ionicToast', '$cordovaDevice', LoginCtrl]);

    function LoginCtrl($scope, $location, MyHTTP, currentUser, ionicToast, $cordovaDevice) {
        var vm = this;
        var VENUE_ID = 1;


        vm.user = {
            venue_id: VENUE_ID,
            email: '',
            password: '',
            device_id: '',
            device_type: '',
            token: ''
        };



        document.addEventListener("deviceready", function() {
            // FCMPlugin.getToken(function(token) {
            //     vm.user.token = token;
            // });
            window.FirebasePlugin.getToken(function(token) {
                // save this server-side and use it to push notifications to this device
                console.log(token);
                vm.user.token = token;
            }, function(error) {
                console.error(error);
            });
            vm.user.device_id = $cordovaDevice.getUUID();
            vm.user.device_type = $cordovaDevice.getPlatform();
        }, false);


        vm.login = function() {
            $scope.spinner = true;
            MyHTTP.post1(vm.user, 'login')
                .then(LoginSuccess)
                .catch(LoginError);
        };


        function LoginSuccess(response) {
            $scope.spinner = false;
            if (response.data.status) {
                currentUser.save(response.data.data);
                currentUser.saveVenueId(response.data.data.venue_id)
                $location.path('app/orders');
            } else {
                $scope.spinner = false;
                ionicToast.show('Email or Password is Incorrect', 'top', false, 4000);
            }
        }

        function LoginError(errorMessage) {
            $scope.spinner = false;
            ionicToast.show('Unable to connect to the server', 'top', false, 4000);


        }


    }

})();