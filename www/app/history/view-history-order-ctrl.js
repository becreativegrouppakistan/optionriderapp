(function() {
  'use strict';

  angular
    .module('MaduberRiderApp')
    .controller('ViewHistoryOrderCtrl', ['OrderServices', '$stateParams', 'setting', 'loading', 'currentUser', '$ionicScrollDelegate', ViewHistoryOrderCtrl]);

  /* @ngInject */
  function ViewHistoryOrderCtrl(OrderServices, $stateParams, setting, loading, currentUser, $ionicScrollDelegate) {


    var vm = this;
    vm.tab= 1;
    vm.title = 'View Order('+$stateParams.order_id+')';

    vm.order = { status: false };
    vm.chatMsgs = {};

    function activate() {
      vm.setting = setting.venue_setting();

      loading.show();
      vm.orders = {};
      OrderServices
        .orders(parseInt(currentUser.profile().rider_id))
        .then(function(resp) {
          vm.orders = resp.data.data;
          angular.forEach(vm.orders, function(option) {
            if ($stateParams.order_id == option.order_id) {
              vm.order = option
              console.log(vm.order);
            }
          });
          loading.hide();
        })
        .catch(function(resp) {
          console.log(resp);
          loading.hide();
        });
    }
    activate();

    vm.statusClass = "";
    vm.orderStatusData = {
      'order_placed': false,
      'reject': false,
      'accept': false,
      'preparing': false,
      'delivery': false,
      'delivered': false
    };
    vm.OrderStatus = function(status) {
      if (status == 0) {
        vm.NotallowFeedback = true;
        vm.orderStatusData = {
          'order_placed': true,
          'reject': false,
          'accept': false,
          'preparing': false,
          'delivery': false,
          'delivered': false
        };

        vm.statusClass = "order_placed";
        return "Order placed";
      } else if (status == 1) {
        vm.NotallowFeedback = true;
        vm.orderStatusData = {
          'order_placed': true,
          'reject': true,
          'accept': false,
          'preparing': false,
          'delivery': false,
          'delivered': false
        };
        vm.statusClass = "reject";
        return "Reject";
      } else if (status == 2) {
        vm.NotallowFeedback = true;
        vm.orderStatusData = {
          'order_placed': true,
          'reject': false,
          'accept': true,
          'preparing': false,
          'delivery': false,
          'delivered': false
        };
        vm.statusClass = "accept";
        return "Accept";
      } else if (status == 3) {
        vm.NotallowFeedback = true;
        vm.orderStatusData = {
          'order_placed': true,
          'reject': false,
          'accept': true,
          'preparing': true,
          'delivery': false,
          'delivered': false
        };
        vm.statusClass = "preparing";
        return "Preparing";
      } else if (status == 4) {
        vm.NotallowFeedback = true;
        vm.orderStatusData = {
          'order_placed': true,
          'reject': false,
          'accept': true,
          'preparing': true,
          'delivery': true,
          'delivered': false
        };
        vm.statusClass = "delivery";
        return "Delivery";
      } else if (status == 5) {

        vm.NotallowFeedback = false;
        vm.orderStatusData = {
          'order_placed': true,
          'reject': false,
          'accept': true,
          'preparing': true,
          'delivery': true,
          'delivered': true
        };
        vm.statusClass = "Delivered";
        return "Delivered";
      }
    };


    vm.OrderPayment = function(payment) {
      if (payment == 0) {
        return 'not paid';
      } else if (payment == 1) {
        return 'paid';
      }
    };

    vm.unreadCount = 0;
    vm.chatmsg = {
      'name': currentUser.profile().name+' (Rider) ',
      'type': 1,
      'msg': '',
      'timestamp': Date.now()
    };
    vm.chatMsgSend = function() {
      firebase.database()
        .ref('chat')
        .child($stateParams.order_id)
        .push(vm.chatmsg);
      $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(true);
      vm.chatmsg.msg = '';
    }

    vm.clearMsgcount = function() {
      vm.unreadCount = 0;
    }

    firebase.database().ref('chat/' + $stateParams.order_id)
      .on('value', function(data) {
        if(vm.tab!==4){
          vm.unreadCount = vm.unreadCount + 1;
        }

        vm.chatMsgs = data.val();
        $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(true);
      });

    vm.chatScrollBottom = function () {
      vm.clearMsgcount();
      $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(true);
    }

//////// map ///////////////////////

    // var venueMarker = new google.maps.Marker();
    // var orderMarker = new google.maps.Marker();
    // var deliveryBoyMarker = new google.maps.Marker();

    vm.initMap = function() {
      var venueLatLng = new google.maps.LatLng(vm.order.venue_address.venue_latitude, vm.order.venue_address.venue_longitude);
      var mapOptions = {
        center: venueLatLng,
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(document.getElementById("map"), mapOptions);
      vm.map = map;

        vm.setDirections(vm.order);

        google.maps.event.addListener(map, 'bounds_changed', function() {
          google.maps.event.trigger(map, 'resize');
        });
      }


      vm.makeMarker = function (position, icon, title, map) {
       var marker = new google.maps.Marker({
          position: position,
          map: map,
          icon: icon,
          title: title
        });
       return marker;
      };



      vm.setDirections = function(order) {
        var start = null;
        var end = null;
        var icons = {
          start: new google.maps.MarkerImage(
            // URL
            'http://maps.google.com/mapfiles/ms/micons/blue.png',
            // (width,height)
            new google.maps.Size(44, 32),
            // The origin point (x,y)
            new google.maps.Point(0, 0),
            // The anchor point (x,y)
            new google.maps.Point(22, 32)),
          end: new google.maps.MarkerImage(
            // URL
            'http://maps.google.com/mapfiles/ms/micons/green.png',
            // (width,height)
            new google.maps.Size(44, 32),
            // The origin point (x,y)
            new google.maps.Point(0, 0),
            // The anchor point (x,y)
            new google.maps.Point(22, 32))
        };

        if(vm.order.venue_address.venue_latitude!==''){
           start = new google.maps.LatLng(vm.order.venue_address.venue_latitude, vm.order.venue_address.venue_longitude);
        }else{
           start = vm.order.venue_address.venue_address;
        }

        if (order.customer_address.latitude!=='' && order.customer_address.longitude!=='') {
          end = new google.maps.LatLng(order.customer_address.latitude , order.customer_address.longitude);
        } else {
          end = order.customer_address.address;
        }

        var rendererOptions = {
          map: vm.map,
          suppressMarkers: true
        };

        var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

        directionsDisplay.setMap(vm.map);
        var request = {
          origin: start,
          destination: end,
          travelMode: 'DRIVING'
        };
        var directionsService = new google.maps.DirectionsService();
        directionsService.route(request, function(response, status) {
          if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);

            var leg = response.routes[0].legs[0];
            vm.start_marker = vm.makeMarker(leg.start_location, icons.start, "title", vm.map);
            vm.end_marker =  vm.makeMarker(leg.end_location, icons.end, 'title', vm.map);

          }
        });
      }
  }
})();
