(function() {
    'use strict';

    angular
        .module('MaduberRiderApp')
        .factory('loading', ['$ionicLoading', loading]);

    /* @ngInject */
    function loading($ionicLoading) {
        var service = {
            show: show,
            hide: hide
        };
        return service;

        ////////////////

        function show() {
            $ionicLoading.show({
                template: '<ion-spinner  icon="spiral" class="spinner-energized"></ion-spinner>'
            });
        }

        function hide() {
            $ionicLoading.hide();
        }
    }
})();